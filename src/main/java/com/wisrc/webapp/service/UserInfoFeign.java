package com.wisrc.webapp.service;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("BASIC-TEST-DEMO")
public interface UserInfoFeign {
    @RequestMapping(value = "/demo/user", method = RequestMethod.GET)
    String getUserInfo();

    @RequestMapping(value = "/demo",method = RequestMethod.GET)
    String getIndex();
}

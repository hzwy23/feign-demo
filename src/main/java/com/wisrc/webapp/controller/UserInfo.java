package com.wisrc.webapp.controller;

import com.wisrc.webapp.service.UserInfoFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserInfo {

    @Autowired
    private UserInfoFeign userInfoFeign;

    @RequestMapping(value = "/user/info", method = RequestMethod.GET)
    public String getUserInfo(){
        return userInfoFeign.getUserInfo();
    }

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String getIndex(){
        return userInfoFeign.getIndex();
    }
}

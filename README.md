# Feign 连接Eureka示例代码

## Feign工程配置文件介绍
```yaml
## defaultZone 指向注册中心
eureka:
  client:
    serviceUrl:
      defaultZone: http://49.4.2.78:8080/eureka

## 应用服务端口
server:
  port: 8765

## 应用示例名称，这个应用将会被注册到注册中心，这里的name就是注册到注册中心的名字
spring:
  application:
    name: service-feign

## 日志级别
logging:
  level:
    root: info
```

## Feign调用示例
Feign 首先在注册中心查找某个应用的详细信息，如在Feign演示项目中，想要访问BASIC-TEST-DEMO这个微服务里边的API，调用流程是：
1. 在服务与发现注册中心查找BASIC-TEST-DEMO这个微服务的机器名，端口号
2. Feign获取到微服务BASIC-TEST-DEMO的机器和端口号后，向这个地址发送http请求，获取响应信息

Feign 使用BASIC-TEST-DEMO的/demo/user这个API的例子是：
```java
// 通过注解的方式，指定微服务注册到注册中心的名称
@FeignClient("BASIC-TEST-DEMO")
public interface UserInfoFeign {
    // 使用RequestMapping这个注册，指定想要访问的微服务里边的API
    @RequestMapping(value = "/demo/user", method = RequestMethod.GET)
    String getUserInfo();
}
```

## 怎么使用Feign定义的接口
```java
@RestController
public class UserInfo {

    @Autowired
    private UserInfoFeign userInfoFeign;

    @RequestMapping(value = "/user/info", method = RequestMethod.GET)
    public String getUserInfo(){
        return userInfoFeign.getUserInfo();
    }
}
```

## 总结
往往一个微服务实例，即是服务提供者，又是服务的消费者。上述的配置信息，仅仅把自己当作是服务的消费者来配置。如果想要把自己的服务提供给其他微服务使用，则把自己注册到服务与发现中心后，指定自己的详细信息，如机器名、端口号。
注册自身时，建议带上下边三个参数：
```yaml
eureka:
  instance:
    prefer-ip-address: true
    ip-address: 49.4.2.78
    instance-id: 49.4.2.78:8790
```
手工指定自己的地址信息。当其他微服务在服务与发现注册中心通过微服务名称查找这个服务时，服务与发现注册中心将会把上边配置的参数返回给查找这个微服务实例的请求方，请求方通过上边的机器地址、端口号向微服务发起请求。